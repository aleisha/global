#!/bin/sh
# This file is part of Koha.
#
# Copyright (C) 2019 Catalyst IT
#
# Koha is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# Koha is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Koha; if not, see <http://www.gnu.org/licenses>.

passengerpid="/var/local/coce/passenger.8080.pid"
if [ ! -f "$passengerpid" ]; then
    echo "PID file ($passengerpid) does not exist"
    cd /var/local/coce
    passenger start --port 8080 --daemonize
fi

PID=`cat $passengerpid`

if ps -p $PID > /dev/null; then
    echo "Passnger is running - do nothing"
else
    cd /var/local/coce
    passenger start --port 8080 --daemonize
fi

if [ ! -f "$passengerpid" ]; then
    warn "The passenger instance is not running"
elif ps -p $PID > /dev/null; then
    warn "The passenger instance is now running"
else
    warn "The passenger instance is not running"
fi

exit 0;

