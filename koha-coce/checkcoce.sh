#!/bin/sh
# This file is part of Koha.
#
# Copyright (C) 2019 Catalyst IT
#
# Koha is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# Koha is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Koha; if not, see <http://www.gnu.org/licenses>.

cocestatus=0
passengerpid="/var/local/coce/passenger.8080.pid"
if [ ! -f "$passengerpid" ]; then
   echo "PID file ($passengerpid) does not exist"
   cocestatus=1
fi

PID=`cat $passengerpid`

if ps -p $PID > /dev/null; then
   echo "$PID is running"
   cocestatus=0
else
   echo "$PID is not running"
   cocestatus=1
fi

echo $cocestatus
exit $cocestatus

