#!/bin/bash
# This file is part of Koha.
#
# Copyright (C) 2017 Catalyst IT
#
# Koha is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# Koha is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Koha; if not, see <http://www.gnu.org/licenses>.

set -e

. /lib/lsb/init-functions

# Read configuration variable file if it is present
[ -r /etc/default/koha-common ] && . /etc/default/koha-common

# include helper functions
if [ -f "/usr/share/koha/bin/koha-functions.sh" ]; then
    . "/usr/share/koha/bin/koha-functions.sh"
else
    echo "Error: /usr/share/koha/bin/koha-functions.sh not present." 1>&2
    exit 1
fi

start_indexer()
{
  local name=$1

  if ! is_indexer_running $name; then
      export KOHA_CONF="/etc/koha/sites/$name/koha-conf.xml"
      DAEMONOPTS="--name=$name-koha-indexer \
                  --errlog=/var/log/koha/$name/indexer-error.log \
                  --stdout=/var/log/koha/$name/indexer.log \
                  --output=/var/log/koha/$name/indexer-output.log \
                  --pidfiles=/var/run/koha/$name/ \
                  --verbose=1 --respawn --delay=30 \
                  --user=$name-koha.$name-koha"

       log_daemon_msg "Starting Koha indexing daemon for $name"

       if daemon $DAEMONOPTS -- $INDEXER_DAEMON $INDEXER_PARAMS; then
            log_end_msg 0
       else
            log_end_msg 1
       fi
  else
       log_daemon_msg "Error: Indexer already running for $name"
       log_end_msg 1
  fi
}

if [ -z $INDEXER_PARAMS ]; then
     # Default to the parameters required by rebuild_zebra.pl
     INDEXER_PARAMS="-daemon -sleep $INDEXER_TIMEOUT"
fi

declare -a zebrainstances
for singleinstancename in $(koha-list --enabled)
do
    if ! is_indexer_running $singleinstancename; then
        zebrainstances+=" $singleinstancename"
        warn "The Zebra indexer for instance $singleinstancename is not running."
    else
        warn "Zebra indexer is running for this instance $singleinstancename."
    fi
done

if [ ! -z "$zebrainstances" ];
then
    for zebrainstance in $zebrainstances 
  do
     instancename=$zebrainstance
     start_indexer $instancename
     warn "Starting Zebra indexer for $instancename"
  done
fi
exit 0;

