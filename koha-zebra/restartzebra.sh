#!/bin/bash
# This file is part of Koha.
#
# Copyright (C) 2017 Catalyst IT
#
# Koha is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# Koha is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Koha; if not, see <http://www.gnu.org/licenses>.

set -e

 # include helper functions
if [ -f "/usr/share/koha/bin/koha-functions.sh" ]; then
   . "/usr/share/koha/bin/koha-functions.sh"
else
   echo "Error: /usr/share/koha/bin/koha-functions.sh not present." 1>&2
   exit 1;
fi

is_zebra_running()
{
     local instancename=$1

     if daemon --name="$instancename-koha-zebra" \
            --pidfiles="/var/run/koha/$instancename/" \
            --running ; then
            return 0
      else
            return 1
      fi
}

start_zebra_instance()
{
 local instancename=$1
 local loglevels=$(get_loglevels $instancename)

 touch "/var/log/koha/$instancename/zebra-error.log" \
     "/var/log/koha/$instancename/zebra-output.log"
 chown "$instancename-koha:$instancename-koha" \
     "/var/log/koha/$instancename/zebra-error.log" \
     "/var/log/koha/$instancename/zebra-output.log" 
 daemon \
     --name="$instancename-koha-zebra" \
     --pidfiles="/var/run/koha/$instancename/" \
     --errlog="/var/log/koha/$instancename/zebra-error.log" \
      --output="/var/log/koha/$instancename/zebra-output.log" \
     --verbose=1 \
     --respawn \
     --delay=30 \
     -- \
     zebrasrv \
      -v $loglevels \
     -f "/etc/koha/sites/$instancename/koha-conf.xml" && \
     return 0 || \
     return 1
}

usage()
{
 local scriptname=$0
     cat <<EOF
 DStarts Zebra for Koha instances.

 Usage: $Scriptname instancename1 instancename2...
EOF
}

declare -a zebrainstances
for singleinstancename in $(koha-list --enabled)
do
    if ! is_zebra_running $singleinstancename; then
        zebrainstances+=" $singleinstancename"
        warn "The Zebra server for instance $singleinstancename is not running."
    else
        warn "Zebra is running for this instance $singleinstancename."
    fi
done

if [ ! -z "$zebrainstances" ];
then
    for zebrainstance in $zebrainstances 
  do
     instancename=$zebrainstance
     start_zebra_instance $instancename
     warn "Starting Zebra server for $instancename"
  done
fi
exit 0;

