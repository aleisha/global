#!/bin/bash
# This file is part of Koha.
#
# Copyright (C) 2023 Catalyst IT
#
# Koha is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# Koha is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Koha; if not, see <http://www.gnu.org/licenses>.

set -e

# include helper functions
if [ -f "/usr/share/koha/bin/koha-functions.sh" ]; then
    . "/usr/share/koha/bin/koha-functions.sh"
else
    echo "Error: /usr/share/koha/bin/koha-functions.sh not present." 1>&2
    exit 1;
fi

declare  -a instancearray
for singleinstancename in $(koha-list --sip --enabled)
do
    if ! is_sip_running $singleinstancename; then
        instancearray+=("false")
    else
        instancearray+=("true")
    fi
done

sipstatus=0
for value in "${instancearray[@]}"
do
    if [ "$value" == "false" ] ; then
        sipstatus=1
    fi
done
echo $sipstatus
exit $sipstatus
